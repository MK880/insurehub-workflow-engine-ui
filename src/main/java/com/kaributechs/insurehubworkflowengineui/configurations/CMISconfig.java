package com.kaributechs.insurehubworkflowengineui.configurations;

import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class CMISconfig {
    @Bean()
    public Session CMISSession(){
        SessionFactory sessionFactory = SessionFactoryImpl.newInstance();
        Map parameter = new HashMap();
        parameter.put(SessionParameter.USER, "admin");
        parameter.put(SessionParameter.PASSWORD, "admin");
//        parameter.put(SessionParameter.ATOMPUB_URL, "http://localhost:8080/alfresco/api/-default-/public/cmis/versions/1.1/atom");
        parameter.put(SessionParameter.ATOMPUB_URL, "http://insurehub.southafricanorth.cloudapp.azure.com:8080/alfresco/api/-default-/public/cmis/versions/1.1/atom");

        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());
        List<Repository> repositories = sessionFactory.getRepositories(parameter);
        Repository repository = repositories.get(0);
        Session session = repository.createSession();
        return session;
    }
}
