package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance;

import lombok.Data;


@Data
public class PersonalVehicleInsuranceQuestions {

    private Long id;
    private String carInsuranceType;
    private String thirdPartyLiability;
    private String bearsAllRisks;
    private String interestedInCourtesyCar;
    private String coversTotalLoss;
    private String coversExcessCosts;
    private String periodOfInsuranceTo;
    private String periodOfInsuranceFrom;
}
