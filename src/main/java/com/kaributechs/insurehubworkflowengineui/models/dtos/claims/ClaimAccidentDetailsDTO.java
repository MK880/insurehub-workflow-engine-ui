package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.Data;

@Data
public class ClaimAccidentDetailsDTO {
    private String driversCondition;
    private String accidentHistory;
    private String accidentHistoryDetails;
    private String driversConditionDetails;
    private String vehiclePurpose;
    private String natureOfInjuries;
    private String accidentDescription;
}
