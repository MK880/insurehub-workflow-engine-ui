package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.Data;
import java.util.Set;

@Data
public class DealerDTO {

    private Long id;
    private String username;
    private String companyName;
    private String mobileNumber;
    private String email;
    private Set<DealerServiceDTO> dealerServices;
    private boolean available;

}
