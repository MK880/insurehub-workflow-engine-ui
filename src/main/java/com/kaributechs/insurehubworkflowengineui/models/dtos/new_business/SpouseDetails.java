package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SpouseDetails {

    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String relationshipToMember;
    private String identificationType;
    private String funeralCover;
}
