package com.kaributechs.insurehubworkflowengineui.models.dtos;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EvidenceDTO {
    private MultipartFile[] files;
    private String processId;
    private Long clientId;
    private String clientFirstName;
    private String clientLastName;
    private String clientIdNumber;
    private String clientType;

    private String businessName;
    private String businessRegistrationNumber;
    private String category;
    private String documentType;
    private String documentSourceChannel;
}
