package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import lombok.Data;

@Data
public class HouseholdInsurancePropertyAddressDTO {

    private boolean isPropertyAddressResidentialAddress;
    private String streetAddress;
    private String suburb;
    private String city;
    private String country;
    private String region;
    private String zipCode;
}
