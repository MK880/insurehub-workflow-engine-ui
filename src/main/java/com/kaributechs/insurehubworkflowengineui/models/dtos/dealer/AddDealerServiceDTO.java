package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class AddDealerServiceDTO {
    @NotNull(message="serviceId cannot be null")
    private Long serviceId;

    @NotNull(message="price cannot be null")
    private BigDecimal price;
}
