package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business;

import lombok.Data;

@Data
public class PersonalDetails {

    private Long id;
    private String salutation;
    private String nationalId;
    private String clientFirstName;
    private String clientLastName;
    private String dateOfBirth;
    private String clientGender;
    private String maritalStatus;
    private String identificationType;
}
