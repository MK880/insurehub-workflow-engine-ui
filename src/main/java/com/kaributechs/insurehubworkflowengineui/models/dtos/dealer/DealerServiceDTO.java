package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class DealerServiceDTO {
    private Long id;
    private String name;
    private BigDecimal price;
}
