package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance;

import lombok.Data;

@Data
public class OtherInsuranceDetailsDTO {
    private String name;
    private String amountOfInsurance;
    private String yearOfIssue;
    private String status;
}
