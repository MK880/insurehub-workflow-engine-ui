package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance;

import lombok.Data;


@Data
public class LifeInsuranceQuestionsDTO {

    private String insuranceType;
    private String benefits;
    private String products;
    private String whenCoverToStart;
    private String premiumFrequency;
    private String grossIncome;
    private String policyObjective;
    private String doInsuredSmoke;
    private String offense;
    private String active;


}
