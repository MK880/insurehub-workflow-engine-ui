package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.company_vehicle_insurance;

import lombok.Data;

import java.util.List;

@Data
public class NewBusinessVehicleDetails {

    private Long id;
    private String registrationNumber;
    private String make;
    private String year;
    private String type;
    private String mileage;
    private String worth;
    private String usage;
    private String numberOfSeats;
    private String alarmSystemYesOrNo;
    private String nightParkingYesOrNo;
    private boolean isVehicleUsedByOthers;
    private String takenHome;


    private VehicleUserDetails vehicleUser;


    private CompanyVehicleInsurance companyVehicleInsurance;


    private List<OtherVehicleUserDetails> otherVehicleUsers;

}
