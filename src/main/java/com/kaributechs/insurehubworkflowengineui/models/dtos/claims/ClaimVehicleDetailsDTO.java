package com.kaributechs.insurehubworkflowengineui.models.dtos.claims;

import lombok.Data;

@Data
public class ClaimVehicleDetailsDTO {
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleModelYear;
    private String vehicleRegistrationNumber;
    private String vehicleMileage;
    private String vehicleUsage;
}
