package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import lombok.Data;

@Data
public class HouseholdInsuranceProtectionDetails {

    private Long id;
    private int daysLeftUnoccupied;
    private boolean isLeftUnoccupiedAtDay;
    private boolean hasDeadlockDevices;
    private boolean isFittedWithSecureLocks;
    private boolean hasBurglarAlarm;
    private boolean isMonitoredAllDay;
    private boolean hasSmokeDetectors;
}
