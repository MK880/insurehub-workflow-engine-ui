package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;


@Data
public class PersonalVehicleInsuranceVehicleDetails {

    private Long id;
    private String vehicleRegistrationNumber;
    private String clientLicenseNumber;
    private String dateWhenLicenseWasObtained;
    private String vehicleMake;
    private String vehicleModel;
    private String vehicleYear;
    private String vehicleMileage;
    private String vehicleWorth;
    private String numberOfSeats;
    private String vehicleHasAlarmSystem;
    private String residentHasNightParking;
    private String numberOfDrivers;

    private PersonalInsuranceSecondaryDriverDetails secondaryDriverDetails;
}
