package com.kaributechs.insurehubworkflowengineui.models.dtos.dealer;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RequestDealerVariablesDTO {
    private Long requestedServiceId;
    private boolean accident;
    private boolean towing;
    private String driverNationalID;
    private String driverSalutation;
    private String driverLastName;
    private String driverFirstName;
    private String driverEmail;
    private String driverPhoneNumber;
    private String location;
    private String serviceType;
    private String preferredServiceDate;
    private String furtherRequests;
    private String vehicleRegistration;
    private String vehicleMake;
    private String vehicleModel;
    private int vehicleYear;
    private String vehicleUsage;
    private Long vehicleMileage;
    private MultipartFile vehicleImage;
    private boolean fixedCar;
    private boolean towedCar;
    private boolean contactedDealer;
    private boolean cancelledRequest;


}
