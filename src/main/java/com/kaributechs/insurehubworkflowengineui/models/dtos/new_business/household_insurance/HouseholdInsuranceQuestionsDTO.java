package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class HouseholdInsuranceQuestionsDTO {
    private BigDecimal fullCostOfHome;
    private BigDecimal fullReplacementCostOfContents;
    private boolean fireOnlyCover;
    private boolean fullAccidentalDamageCover;
    private boolean optionalCoverForJewellery;
    private Date periodOfInsuranceFrom;
    private Date periodOfInsuranceTo;
    private boolean optionalCoverOnIndividualItems;
}
