package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.household_insurance;

import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.ContactDetails;
import lombok.Data;

import java.util.Set;

@Data
public class HouseholdInsurance {

    private Long id;

    private String processId;

    private HouseholdInsuranceMemberDetails memberDetails;

    private ContactDetails contactDetails;

    private Set<HouseholdInsurancePropertyDetails> properties;

    private Set<HouseholdInsuranceChildrenDetails> children;

    private Set<HouseholdInsuranceExtendedFamilyDetails> extendedFamily;

    private Set<HouseholdInsuranceBeneficiaryDetails> beneficiaries;

    private HouseholdInsuranceQuestions insuranceQuestions;
}
