package com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import java.util.Date;

@Data
public class LifeInsuranceExtendedFamilyDetails {

    private Long id;
    private String lastName;
    private String firstName;
    private String salutation;
    private String nationalId;
    private Date dateOfBirth;
    private String emailAddress;
    private String phoneNumber;
    private String gender;
    private String identificationType;
    private String relationshipToMember;
    private String maritalStatus;
    private String funeralCover;

}
