package com.kaributechs.insurehubworkflowengineui.services;


import com.kaributechs.insurehubworkflowengineui.exceptions.GenericException;
import com.kaributechs.insurehubworkflowengineui.models.dtos.CMISObjectDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.FolderDTO;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FoldersServiceImp implements IFoldersService {
    Logger logger= LoggerFactory.getLogger(this.getClass());
    private final Session session;

    public FoldersServiceImp(Session session) {
        this.session = session;
    }

    @Override
    public List<CMISObjectDTO> getFoldersImp() {
        Folder root = session.getRootFolder();
        ItemIterable<CmisObject> children = root.getChildren();

        return mapCMISObjects(children);
    }

    @Override
    public FolderDTO createFolderImp(FolderDTO folderDTO) {
        Map<String, String> newFolderProps = new HashMap<String, String>();
        newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
        newFolderProps.put(PropertyIds.NAME, folderDTO.getName());
        try {
            Folder newFolder=session.getRootFolder().createFolder(newFolderProps);
            return new FolderDTO(newFolder.getId(),newFolder.getName(),newFolder.getType().getDisplayName(),newFolder.getPath());
        }catch (Exception e){
            throw new GenericException(e.getMessage());
        }
    }

    List<CMISObjectDTO> mapCMISObjects(ItemIterable<CmisObject> cmisObjects){
        List<CMISObjectDTO> objects=new ArrayList<>();
        for (CmisObject o : cmisObjects) {
            objects.add(new CMISObjectDTO(o.getId(),o.getName(),o.getType().getDisplayName()));
        }
        return objects;
    }
}
