package com.kaributechs.insurehubworkflowengineui.services;

import com.kaributechs.insurehubworkflowengineui.models.dtos.AddEvidenceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.DocumentDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.EvidenceDTO;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

public interface IEvidenceService {
    List<DocumentDTO> uploadEvidenceImp(EvidenceDTO files) throws IOException;

    DocumentDTO getEvidenceByIdImp(String id) throws IOException;

    ResponseEntity<byte[]> downloadEvidenceImp(String id) throws IOException;

    List<DocumentDTO> getEvidenceByClaimIdImp(String processId) throws IOException;

    List<DocumentDTO> addEvidenceImp(AddEvidenceDTO addEvidenceDTO) throws IOException;

    void deleteEvidenceImp(String id);
}
