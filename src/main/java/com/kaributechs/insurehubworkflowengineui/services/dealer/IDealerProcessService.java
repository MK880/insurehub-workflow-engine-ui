package com.kaributechs.insurehubworkflowengineui.services.dealer;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.TaskDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.CompleteDealerTaskDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.RequestDealerVariablesDTO;

import java.io.IOException;

public interface IDealerProcessService {
    ProcessInstanceDTO startDealerProcessImp(RequestDealerVariablesDTO requestDealerDTO) throws IOException;

    TaskDTO completeDealerTask(String taskId, CompleteDealerTaskDTO completeDealerTaskDTO);
}
