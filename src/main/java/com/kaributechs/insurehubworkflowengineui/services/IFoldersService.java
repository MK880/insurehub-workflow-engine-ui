package com.kaributechs.insurehubworkflowengineui.services;



import com.kaributechs.insurehubworkflowengineui.models.dtos.CMISObjectDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.FolderDTO;

import java.util.List;

public interface IFoldersService {
    List<CMISObjectDTO> getFoldersImp();

    FolderDTO createFolderImp(FolderDTO folderDTO);

}
