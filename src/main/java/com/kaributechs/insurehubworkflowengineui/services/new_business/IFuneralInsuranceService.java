package com.kaributechs.insurehubworkflowengineui.services.new_business;

import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsuranceDTO;

public interface IFuneralInsuranceService {
    ProcessInstanceDTO apply(FuneralInsuranceDTO funeralInsuranceDTO);

    FuneralInsurance getInsuranceByProcessId(String processId);
}
