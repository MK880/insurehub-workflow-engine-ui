package com.kaributechs.insurehubworkflowengineui.services;

import com.kaributechs.insurehubworkflowengineui.models.dtos.claims.ClaimsWorkflowRequestDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessDefinitionDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.ResponseDTO;

import java.io.IOException;
import java.util.List;

public interface IActivitiProcessServices {
    List<ProcessDefinitionDTO> getAllProcessesImp();

    ResponseDTO resumeProcessByIdImp(String processInstanceId);

    ProcessInstanceDTO startProcessByIdImp(String processId, ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO);

    List<ProcessInstanceDTO> getRunningProcessesImp();

    ResponseDTO suspendProcessImp(String processId);

    ResponseDTO activateProcessImp(String processId);

    ResponseDTO stopProcessByIdImp(String processInstanceId);

    ProcessInstanceDTO startClaimsProcess(ClaimsWorkflowRequestDTO claimsWorkflowRequestDTO) throws IOException;
}
