package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.personal_vehicle_insurance.PersonalVehicleInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.IPersonalVehicleInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/personal-vehicle-insurance")
@Tag(name = "Personal vehicle insurance API endpoints", description = "Endpoints for Personal vehicle insurance only")
@Slf4j
public class PersonalVehicleInsuranceController {
    private final IPersonalVehicleInsuranceService personalVehicleInsuranceService;

    public PersonalVehicleInsuranceController(IPersonalVehicleInsuranceService personalVehicleInsuranceService) {
        this.personalVehicleInsuranceService = personalVehicleInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for personal vehicle insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid PersonalVehicleInsuranceDTO personalVehicleInsuranceDTO){
        return personalVehicleInsuranceService.apply(personalVehicleInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get personal vehicle insurance details by process id")
    public PersonalVehicleInsurance getInsuranceByProcessId(@PathVariable String processId){
        return personalVehicleInsuranceService.getInsuranceByProcessId(processId);
    }
}
