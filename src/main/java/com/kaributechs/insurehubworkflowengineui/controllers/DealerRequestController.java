package com.kaributechs.insurehubworkflowengineui.controllers;

import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.dealer.DealerRequestDTO;
import com.kaributechs.insurehubworkflowengineui.services.dealer.IDealerRequestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/dealer-request")
@Tag(name = "Find A Dealer Process API endpoints", description = "Endpoints for the find a dealer process only")
@Slf4j
public class DealerRequestController {
    private final IDealerRequestService dealerRequestService;

    public DealerRequestController(IDealerRequestService dealerRequestService) {
        this.dealerRequestService = dealerRequestService;
    }

    @GetMapping("/{processId}")
    @Operation(summary = "Get dealer request by process id")
    public DealerRequestDTO getDealerRequestByProcessId(@PathVariable String processId){
        return dealerRequestService.getDealerRequestByProcessId(processId);
    }
}
