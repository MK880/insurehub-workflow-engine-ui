package com.kaributechs.insurehubworkflowengineui.controllers;


import com.kaributechs.insurehubworkflowengineui.models.dtos.ProcessInstanceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.funeral_insurance.FuneralInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsurance;
import com.kaributechs.insurehubworkflowengineui.models.dtos.new_business.life_insurance.LifeInsuranceDTO;
import com.kaributechs.insurehubworkflowengineui.services.new_business.IFuneralInsuranceService;
import com.kaributechs.insurehubworkflowengineui.services.new_business.ILifeInsuranceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/funeral-insurance")
@Tag(name = "Funeral insurance API endpoints", description = "Endpoints for funeral insurance only")
@Slf4j
public class FuneralInsuranceController {
    private final IFuneralInsuranceService lifeInsuranceService;

    public FuneralInsuranceController(IFuneralInsuranceService lifeInsuranceService) {
        this.lifeInsuranceService = lifeInsuranceService;
    }

    @PostMapping("/apply")
    @Operation(summary = "Apply for funeral insurance")
    public ProcessInstanceDTO apply(@RequestBody @Valid FuneralInsuranceDTO funeralInsuranceDTO){
        return lifeInsuranceService.apply(funeralInsuranceDTO);
    }

    @GetMapping("/get/{processId}")
    @Operation(summary = "Get life insurance details by process id")
    public FuneralInsurance getInsuranceByProcessId(@PathVariable String processId){
        return lifeInsuranceService.getInsuranceByProcessId(processId);
    }
}
